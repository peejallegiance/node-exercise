# init npm
npm init 

# install express
npm install express


# or use npx 
npx express-generator


# change users route to people
line 8 and 25 of app.js
var peopleRouter = require('./routes/people');
app.use('/people', peopleRouter);


# add planets route 
var planetsRouter = require('./routes/planets');
app.use('/planets', planetsRouter);


# restart express server
npm start 


# install axios 
npm install axios

# include axios for use in our route
var axios = require('axios');

# get swapi data using async / await 

/* GET people listing from Swapi using axios. */
router.get('/', async (req, res, next) => {
  try {
    const response = await axios.get("https://swapi.dev/api/people")
    while(res.next != null){
        res.json(response.data)
    }
  }
  catch (err) {
      console.log(err)
  }
});



# check for query params and perform various sorts 

    //check for query params 
    if(req.query){
        let sortType = req.query.sortBy;
        console.log("sorting by " + sortType + "...")
  
        //use a slightly different sort for names
        if(sortType == "name"){
          console.log("using localeCompare")
          people.sort((a, b) => a.name.localeCompare(b.name))
        }
  
        //otherwise use a regular sort
        else{
          people.sort((a, b) => a[sortType] - b[sortType])
        }
      }
