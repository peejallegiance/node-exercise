var express = require('express');
var router = express.Router();
var axios = require('axios');

/* GET people listing from Swapi using axios. */
router.get('/', async (req, res, next) => {
  console.log(req.query.sortBy);
  try {
    let populate = "https://swapi.dev/api/people";
    let people = [];

    //iterate over paginated results until we have them all 
    while(populate){
      let nextPage = await axios.get(populate);
      const { next, results } = await nextPage.data;
      populate = next;
      people = [...people, ...results];
    }

    //check for query params 
    if(req.query){
      let sortType = req.query.sortBy;
      console.log("sorting by " + sortType + "...")

      //use a slightly different sort for names
      if(sortType == "name"){
        console.log("using localeCompare")
        people.sort((a, b) => a.name.localeCompare(b.name))
      }

      //otherwise use a regular sort
      else{
        people.sort((a, b) => a[sortType] - b[sortType])
      }
    }
    res.send(people)
  }
  catch (err) {
      console.log(err)
  }
});

module.exports = router;
