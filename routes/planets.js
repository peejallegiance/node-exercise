var express = require('express');
var router = express.Router();
var axios = require('axios');

const middle = (req, res, next) => {
    console.log("middleware" + req.body)
    next()
}
/* GET planets listing. */
router.get('/', middle, async (req, res, next) => {
    console.log("getting all planets...")
    try {
      let populate = "https://swapi.dev/api/planets";
      let planets = [];
  
      //iterate over paginated results until we have them all 
      while(populate){
        let nextPage = await axios.get(populate);
        const { next, results } = await nextPage.data;
        populate = next;
        planets = [...planets, ...results];
      }
      res.send(planets)
    }
    catch (err) {
        console.log(err)
    }
  });
  
  module.exports = router;